package io;

import carte.Carte;
import incendie.Incendie;
import robots.Robot;

import java.util.List;
import java.util.Set;

public class DonneesSimulation {
    private final Carte carte;
    private final Set<Incendie> incendies;
    private final List<Robot> robots;

    /**
     * Durée en seconde de la simulation depuis son lancement
     */
    private int temps;

    public DonneesSimulation(Carte carte, Set<Incendie> incendies, List<Robot> robots) {
        this.carte = carte;
        this.incendies = incendies;
        this.robots = robots;
        temps = 0;
    }
}
