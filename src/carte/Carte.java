package carte;

public class Carte {
    private final Case[][] grille;

    /**
     * Longueur et largeur d'une case en mètres
     */
    private final int tailleCases;

    public Carte(Case[][] grille, int tailleCases) {
        this.grille = grille;
        this.tailleCases = tailleCases;
    }

    public int getNbLignes() {
        return grille.length;
    }

    public int getNbColonnes() {
        if (grille.length == 0) return 0;
        return grille[0].length;
    }

    public int getTailleCases() {
        return tailleCases;
    }

    public Case getCase(int ligne, int colonne) {
        return grille[ligne][colonne];
    }

    public boolean voisinExiste(Case source, Direction direction) {
        return (direction == Direction.EST && source.getColonne() < getNbColonnes() - 1)
                || (direction == Direction.OUEST && source.getColonne() > 0)
                || (direction == Direction.SUD && source.getLigne() < getNbLignes() - 1)
                || (direction == Direction.NORD && source.getLigne() > 0);
    }

    public Case getVoisin(Case source, Direction direction) {
        if (!voisinExiste(source, direction)) throw new IllegalArgumentException("Cette case n'a pas de voisin" +
                " dans cette direction");
        if (direction == Direction.EST) return grille[source.getLigne() + 1][source.getColonne()];
        if (direction == Direction.OUEST) return grille[source.getLigne() - 1][source.getColonne()];
        if (direction == Direction.SUD) return grille[source.getLigne()][source.getColonne() - 1];
        if (direction == Direction.NORD) return grille[source.getLigne()][source.getColonne() + 1];
        throw new IllegalArgumentException("Une direction doit être EST, OUEST, SUD ou NORD");
    }
}
