package carte;

public class Case {
    private final int ligne;
    private final int colonne;
    private final NatureTerrain natureTerrain;

    public Case(int ligne, int colonne, NatureTerrain natureTerrain) {
        this.ligne = ligne;
        this.colonne = colonne;
        this.natureTerrain = natureTerrain;
    }

    public int getLigne() {
        return ligne;
    }

    public int getColonne() {
        return colonne;
    }

    public NatureTerrain getNature() {
        return natureTerrain;
    }
}
