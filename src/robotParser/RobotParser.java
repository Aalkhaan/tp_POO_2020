package robotParser;

import carte.Case;
import robots.*;

public class RobotParser {
    public static Robot parse(String string, Case position) {
        if (string.equals("DRONE")) return new Drone(position);
        if (string.equals("ROUES")) return new Roues(position);
        if (string.equals("CHENILLES")) return new Chenilles(position);
        if (string.equals("PATTES")) return new Pattes(position);
        throw new IllegalArgumentException("Format incorrect");
    }

    public static Robot parse(String string, Case position, int vitesse) {
        if (string.equals("DRONE")) return new Drone(position, vitesse);
        if (string.equals("ROUES")) return new Roues(position, vitesse);
        if (string.equals("CHENILLES")) return new Chenilles(position, vitesse);
        throw new IllegalArgumentException("Format incorrect");
    }
}
