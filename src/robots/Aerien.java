package robots;

import carte.Case;
import carte.NatureTerrain;

import java.util.Arrays;
import java.util.HashSet;

public abstract class Aerien extends Robot {
    public Aerien(Case position) {
        super(position);
        setAccessibles(new HashSet<>(Arrays.asList(NatureTerrain.values())));
    }

    @Override
    public double getVitesse(NatureTerrain natureTerrain) {
        return getVitesse();
    }
}
