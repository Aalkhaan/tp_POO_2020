package robots;

import carte.Case;
import carte.NatureTerrain;
import incendie.Incendie;

import java.util.Arrays;
import java.util.HashSet;

public class Pattes extends Terrestre {
    int volumeUnitaire = 10;

    public Pattes(Case position) {
        super(position);
        setVitesse(30);
        HashSet<NatureTerrain> accessibles = new HashSet<>(Arrays.asList(NatureTerrain.values()));
        accessibles.remove(NatureTerrain.EAU);
        setAccessibles(accessibles);
    }

    @Override
    public double getVitesse(NatureTerrain natureTerrain) {
        if (natureTerrain == NatureTerrain.ROCHE) return 10;
        return getVitesse();
    }

    @Override
    public int deverserEau(Incendie incendie) {
        incendie.seFaireArroser(volumeUnitaire);
        return 1;
    }

    @Override
    public int remplirReservoir() {
        return 0;
    }
}
