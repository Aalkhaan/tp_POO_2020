package robots;

import carte.Case;
import carte.NatureTerrain;
import incendie.Incendie;

import java.util.HashSet;

public class Roues extends Terrestre {
    int volumeUnitaire = 100;

    public Roues(Case position) {
        super(position);
        setVitesse(80);
        setCapacite(5000);
        setVolume(getCapacite());
        HashSet<NatureTerrain> accessibles = new HashSet<>();
        accessibles.add(NatureTerrain.TERRAIN_LIBRE);
        accessibles.add(NatureTerrain.HABITAT);
        setAccessibles(accessibles);
    }

    public Roues(Case position, double vitesse) {
        super(position);
        if (vitesse <= 0) throw new IllegalArgumentException("La vitesse doit être strictement positive");
        setVitesse(vitesse);
        HashSet<NatureTerrain> accessibles = new HashSet<>();
        accessibles.add(NatureTerrain.TERRAIN_LIBRE);
        accessibles.add(NatureTerrain.HABITAT);
        setAccessibles(accessibles);
    }

    @Override
    public double getVitesse(NatureTerrain natureTerrain) {
        return 0;
    }

    @Override
    public int deverserEau(Incendie incendie) {
        setVolume(getVolume() - volumeUnitaire);
        incendie.seFaireArroser(volumeUnitaire);
        return 5;
    }

    @Override
    public int remplirReservoir() {
        setVolume(getCapacite());
        return 10 * 60;
    }
}
