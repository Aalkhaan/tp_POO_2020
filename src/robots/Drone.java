package robots;

import carte.Case;
import carte.NatureTerrain;
import incendie.Incendie;

public class Drone extends Aerien {
    int volumeUnitaire;

    public Drone(Case position) {
        super(position);
        setVitesse(100);
        setCapacite(10000);
        volumeUnitaire = getCapacite();
        setVolume(getCapacite());
    }

    public Drone(Case position, double vitesse) {
        super(position);
        if (vitesse > 150) throw new IllegalArgumentException("Un drone ne peut pas se déplacer à plus de 150km/h");
        setVitesse(vitesse);
        setCapacite(10000);
        volumeUnitaire = getCapacite();
        setVolume(getCapacite());
    }

    @Override
    public int deverserEau(Incendie incendie) {
        setVolume(getCapacite() - volumeUnitaire);
        incendie.seFaireArroser(volumeUnitaire);
        return 30;
    }

    @Override
    public int remplirReservoir() {
        if (getPosition().getNature() == NatureTerrain.EAU) {
            setVolume(getCapacite());
            return 30 * 60;
        }
        throw new IllegalArgumentException("Le drone ne peut se remplir qu'au dessus de l'eau");
    }
}
