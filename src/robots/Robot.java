package robots;

import carte.Case;
import carte.NatureTerrain;
import incendie.Incendie;

import java.util.Set;

public abstract class Robot {
    private Case position;

    /**
     * Vitesse en km/h
     */
    private double vitesse;

    /**
     * Capacité d'eau du robot
     */
    private int capacite;

    /**
     * Quantité d'eau contenue dans le réservoir en litres
     */
    private int volume;

    private Set<NatureTerrain> accessibles;

    public Robot(Case position) {
        this.position = position;
    }

    public Case getPosition() {
        return position;
    }

    public void setPosition(Case position) {
        if (!accessibles.contains(position.getNature())) throw new IllegalArgumentException("Terrain non praticable" +
                " pour ce robot");
        if (!(  (this.position.getLigne() == position.getLigne()
                && Math.abs(this.position.getColonne() - position.getColonne()) <= 1)
                || (Math.abs(this.position.getLigne() - position.getLigne()) <= 1
                && this.position.getColonne() == position.getColonne())))
            throw new IllegalArgumentException("La case de destination n'est pas adjacente à celle ci");
        this.position = position;
    }

    public abstract double getVitesse(NatureTerrain natureTerrain);

    public abstract int deverserEau(Incendie incendie);

    /**
     * Remplit le reservoir si de l'eau est à proximité
     * @return la durée de l'opération en secondes
     */
    public abstract int remplirReservoir();

    public int getVolume() {
        return volume;
    }

    public double getVitesse() {
        return vitesse;
    }

    void setVitesse(double vitesse) {
        this.vitesse = vitesse;
    }

    public int getCapacite() {
        return capacite;
    }

    void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    void setVolume(int volume) {
        this.volume = volume;
    }

    void setAccessibles(Set<NatureTerrain> accessibles) {
        this.accessibles = accessibles;
    }

    public Set<NatureTerrain> getAccessibles() {
        return accessibles;
    }
}
