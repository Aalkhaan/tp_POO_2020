package robots;

import carte.Case;
import carte.NatureTerrain;
import incendie.Incendie;

import java.util.Arrays;
import java.util.HashSet;

public class Chenilles extends Terrestre {
    int volumeUnitaire = 100;

    public Chenilles(Case position) {
        super(position);
        setVitesse(60);
        setCapacite(2000);
        setVolume(getCapacite());
        HashSet<NatureTerrain> accessibles = new HashSet<>(Arrays.asList(NatureTerrain.values()));
        accessibles.remove(NatureTerrain.EAU);
        accessibles.remove(NatureTerrain.ROCHE);
        setAccessibles(accessibles);
    }

    public Chenilles(Case position, double vitesse) {
        super(position);
        if (vitesse <= 0) throw new IllegalArgumentException("La vitesse doit être strictement positive");
        if (vitesse > 80) throw new IllegalArgumentException("Un robot à chenilles ne peut pas se déplacer à plus" +
                " de 80km/h");
        setVitesse(vitesse);
        HashSet<NatureTerrain> accessibles = new HashSet<>(Arrays.asList(NatureTerrain.values()));
        accessibles.remove(NatureTerrain.EAU);
        accessibles.remove(NatureTerrain.ROCHE);
        setAccessibles(accessibles);
    }

    @Override
    public double getVitesse(NatureTerrain natureTerrain) {
        if (natureTerrain == NatureTerrain.FORET) return getVitesse() / 2;
        return getVitesse();
    }

    @Override
    public int deverserEau(Incendie incendie) {
        setVolume(getVolume() - volumeUnitaire);
        incendie.seFaireArroser(volumeUnitaire);
        return 8;
    }

    @Override
    public int remplirReservoir() {
        setVolume(getCapacite());
        return 5 * 60;
    }
}
