package incendie;

import carte.Case;

public class Incendie {
    private final Case aCase;
    private int litresEau;

    public Incendie(Case aCase, int litresEau) {
        this.aCase = aCase;
        this.litresEau = litresEau;
    }

    public void seFaireArroser(int nbLitres) {
        litresEau -= nbLitres;
    }

    public boolean estEteint() {
        return litresEau <= 0;
    }
}
